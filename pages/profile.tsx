import { NextPage } from 'next';
import * as React from "react";
import styles from "./profile.module.css";
import {useEffect, useState} from "react";
import {ApiManager} from "../services/ApiManager";
import {Router, useRouter} from "next/router";

const Profile: NextPage = () => {
    const router = useRouter();
    const [username, setUsername] = useState<string>('');
    const [newPassword, setNewPassword] = useState<string>('');
    const [repeatNewPassword, setRepeatNewPassword] = useState<string>('');
    const updateProfile = () => {
        if (username && ((newPassword && newPassword === repeatNewPassword) || (!newPassword && !repeatNewPassword))) {
            ApiManager.updateUser({
                username: username,
                password: newPassword ? newPassword : undefined
            })
                .catch((res) => {
                    router.push('/login');
                });
        }
        else alert('You cannot give an empty username or unequal new password fields!');
    };

    useEffect(() => {
        if (!ApiManager.isAuthenticated()) {
            router.push('/login');
            return;
        }

        ApiManager.getUserName().then(res => {
            setUsername(res.body?.name || '');
        }).catch(() => alert('Oops something went wrong! Please reload the page'));
    }, []);

    return (
        <div className={styles['container']}>
            <div className={styles['profile-container']}>
                <label className={styles['input-container']}>
                    <span className={styles['label']}>Username:</span>
                    <input type="text" placeholder="Username"
                           className={styles['input']}
                           value={username}
                           onChange={(e) => setUsername(e.target.value)}
                   />
                </label>
                <label className={styles['input-container']}>
                    <span className={styles['label']}>New password:</span>
                    <input type="password" placeholder="New password"
                           className={styles['input']}
                           value={newPassword}
                           onChange={(e) => setNewPassword(e.target.value)}
                   />
                </label>
                <label className={styles['input-container']}>
                    <span className={styles['label']}>Repeat new password:</span>
                    <input type="password" placeholder="Repeat new password"
                           className={styles['input']}
                           value={repeatNewPassword}
                           onChange={(e) => setRepeatNewPassword(e.target.value)}
                   />
                </label>
                <button className={styles['update-btn']} onClick={updateProfile}>Update</button>
            </div>
        </div>
    );
};

export default Profile;
