import { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import * as React from "react";
import BarGraph from "../components/Charts/BarChartMonth";
import {useEffect, useState} from "react";
import {ApiManager} from "../services/ApiManager";
import {SensorDataDto} from "../models/dto/SensorDataDto";
import Link from 'next/link';
import {SensorTypeEnum} from "../models/SensorTypeEnum";
import RealtimeLineChart from '../components/Charts/RealtimeLineChart';

const Home: NextPage = () => {
    const [sensorData, setSensorData] = useState<SensorDataDto[]>([]);
    const [avgWattMeasurement, setAvgWattMeasurement] = useState<number|undefined>();
    const getDecimalCount = (number: number) => {
        const count = -Math.floor( Math.log10(number)+1);
        if(count == Infinity) return 0;
        return Math.max(0, count);
    };
    const getAvgWattMeasurement = (sensorDataIn: SensorDataDto[]) => {
        let wattMeasurementAcumulator = 0;
        let deltaTimeAcumulator = 0;
        const currentDate = new Date();
        sensorDataIn.map(data => {
            data.createdAt = new Date(data.createdAt);
            return data;
        }).sort(data => data.createdAt.getTime()).forEach((data, i) => {
            if(data.sensor.typeName !== SensorTypeEnum.ElectricCurrent) return;

            let deltaTime = 0;
            if(i != 0 && currentDate.getTime() - data.createdAt.getTime() < 1000*60*60*24*7*4.333) deltaTime += (data.createdAt.getTime() - sensorDataIn[i-1].createdAt.getTime()) / 2;
            if(i != sensorDataIn.length-1 && currentDate.getTime() - data.createdAt.getTime() < 1000*60*60*24*30) deltaTime += (sensorDataIn[i+1].createdAt.getTime() - data.createdAt.getTime()) / 2;

            wattMeasurementAcumulator += data.measurement*data.contextUnit;
            deltaTimeAcumulator += deltaTime;
        });


        if(deltaTimeAcumulator == 0) return wattMeasurementAcumulator;
        return wattMeasurementAcumulator/deltaTimeAcumulator;
    };
    useEffect(() => {
        ApiManager.getSensorData()
            .then((res) => {
                setSensorData(res.body || []);
            });
    }, []);

    useEffect(() => {
        const avgWattMeasurement = getAvgWattMeasurement(sensorData);
        setAvgWattMeasurement(avgWattMeasurement/1800000);
    });

    return (
        <main className={styles.main}>
            <h1 className={styles.title}>Welkom op onze dashboard!</h1>

            {typeof avgWattMeasurement === 'number' && <div>
                Avg measurement of the last 30 days: {avgWattMeasurement.toFixed(getDecimalCount(avgWattMeasurement)+3)}kWh<br></br>
                Predicts a cost of €{(avgWattMeasurement*(60/2)*60*24*7*4.333*0.25).toFixed(2)} this month
            </div>}

            <BarGraph/>
            <RealtimeLineChart />
        </main>
    );
};

export default Home;
