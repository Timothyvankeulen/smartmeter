import "../styles/globals.css";
import { AppProps } from "next/app";

// add bootstrap css
import "bootstrap/dist/css/bootstrap.css";
import Layout from "../components/Layout";

function MyApp({ Component, pageProps }: AppProps) {
  return (// @ts-ignore
      <Layout>
        <Component {...pageProps} />
      </Layout>
  );
}

export default MyApp;
