import { NextPage } from 'next';
import * as React from "react";
import styles from "./edit-sensors.module.css";
import {useEffect, useState} from "react";
import {ApiManager} from "../services/ApiManager";
import {useRouter} from "next/router";
import {SensorDto} from "../models/dto/SensorDto";

const EditSensors: NextPage = () => {
    const router = useRouter();
    const [sensors, setSensors] = useState<SensorDto[]>([]);
    const [editableSensorLabel, setEditableSensorLabel] = useState<string>('');
    const [editableSensorContextUnit, setEditableSensorContextUnit] = useState<number>(0);
    const [editIdx, setEditIdx] = useState<number>(-1);
    const fetchSensors = (callback?: ()=>void) => {
        ApiManager.getSensors()
            .then(res => {
                setSensors(res.body || []);
                if(callback) callback();
            }).catch(() => alert('Oops something went wrong! Please reload the page'));
    };
    const edit = (idx: number) => {
        setEditIdx(idx);
        setEditableSensorLabel(sensors[idx].label);
        setEditableSensorContextUnit(sensors[idx].contextUnit);
    };
    const update = () => {
        if(!editableSensorLabel || !editableSensorContextUnit) {
            alert('Label and contextUnit cannot be empty');
            return;
        }

        ApiManager.updateSensor(sensors[editIdx].id,{
            label: editableSensorLabel,
            contextUnit: editableSensorContextUnit
        })
            .then(() => {
                fetchSensors(() => {
                    setEditIdx(-1);
                });
            })
            .catch((res) => {
                router.push('/login');
            });
    };

    useEffect(() => {
        if (!ApiManager.isAuthenticated()) {
            router.push('/login');
            return;
        }

        fetchSensors();
    }, []);

    return (
        <div className={styles['container']}>
            <table className={styles['table']}>
                <thead className={styles['thead']}>
                    <tr>
                        <th className={styles['thead-id']}>ID:</th>
                        <th className={styles['thead-label']}>Label</th>
                        <th className={styles['thead-context-unit']}>contextUnit</th>
                    </tr>
                </thead>
                <tbody className={styles['tbody']}>
                    {sensors.map((sensor, idx) => {
                        const isSelected = editIdx == idx;
                        return (
                            <tr key={idx}>
                                <td>{sensor.id}</td>
                                <td>
                                    <input type="text"
                                           value={isSelected ? editableSensorLabel : sensor.label}
                                           className={styles['input']}
                                           disabled={!isSelected}
                                           onChange={(e) => setEditableSensorLabel(e.target.value)}
                                    />
                                </td>
                                <td>
                                    <input type="number"
                                           value={isSelected ? editableSensorContextUnit : sensor.contextUnit}
                                           className={styles['input']}
                                           disabled={!isSelected}
                                           onChange={(e) => setEditableSensorContextUnit(Number(e.target.value))}
                                    />
                                </td>
                                <td>
                                    {!isSelected ?
                                        <button className={styles['update-btn']} onClick={() => edit(idx)}>Edit</button>
                                        : <button className={styles['update-btn']} onClick={update}>Update</button>
                                    }
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default EditSensors;
