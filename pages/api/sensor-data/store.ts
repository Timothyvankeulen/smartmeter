// TODO: insert data /w sensorId, measurement & contextUnit of the sensorId at that time; only allowed from localhost
import { User, SensorData } from "@prisma/client";
import {NextApiRequest, NextApiResponse} from "next";
import {ResponseDto} from "../../../models/dto/ResponseDto";
import {UserDto} from "../../../models/dto/UserDto";
import {prisma} from "../../../services/prisma";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<UserDto>>
) {
    if (req.method != "POST") return res.status(405).setHeader("Allow", "POST").end();
    if (typeof req.body.sensorId !== 'number'
     || typeof req.body.measurement !== 'number'
    ) return res.status(400).end();

    const ip = req.headers["x-real-ip"] || req.connection.remoteAddress;
    if(ip != '::1') return res.status(403).end();


    const sensor = await prisma.sensor.findUnique({where: {id: req.body.sensorId}});
    if(sensor === null) return res.status(409).json({
        status: 409,
        ok: false
    });

    await prisma.sensorData.create({
        data: {
            sensorId: req.body.sensorId,
            measurement: req.body.measurement,
            contextUnit: sensor.contextUnit
        }
    });

    res.status(200).json({
        status: 200,
        ok: true
    });
}
