// TODO: get sensorLabel, sensorType, measurement, contextUnit & createAt /w or /w-out sensorId
import { SensorData } from "@prisma/client";
import {NextApiRequest, NextApiResponse} from "next";
import {ResponseDto} from "../../../models/dto/ResponseDto";
import {prisma} from '../../../services/prisma';
import {SensorDataDto} from "../../../models/dto/SensorDataDto";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<SensorDataDto[]>>
) {
    if (req.method != "GET") return res.status(405).setHeader("Allow", "GET").end();


    const sensorId = Number(req.query.sensorId);
    if (!(!isNaN(sensorId) || typeof req.query.sensorId === 'undefined')) return res.status(400).end();

  const sensorData = await prisma.sensorData.findMany({
    where: { sensorId: isNaN(sensorId) || !req.query.sensorId ? undefined : sensorId },
    include: { sensor: true },
  });

    res.status(200).json({
        status: 200,
        ok: true,
        body: sensorData as SensorDataDto[]
    })
}