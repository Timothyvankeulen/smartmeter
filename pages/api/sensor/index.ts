// TODO: get id, label, contextUnit & type /w sensorId
import { NextApiRequest, NextApiResponse } from 'next';
import {Sensor } from '@prisma/client';
import {ResponseDto} from "../../../models/dto/ResponseDto";
import {SensorDto} from "../../../models/dto/SensorDto";
import { prisma } from '../../../services/prisma';

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<SensorDto[]>>
) {
    if (req.method != "GET") return res.status(405).setHeader("Allow", "GET").end();
    if (!(!isNaN(req.body.sensorId) || typeof req.body.sensorId === 'undefined')) return res.status(400).end();

    let data: SensorDto[];
    if(isNaN(req.body.sensorId)) {
        data = (await prisma.sensor.findMany()).map(sensor => {
            return {
                id: sensor.id,
                label: sensor.label,
                contextUnit: sensor.contextUnit,
                type: sensor.typeName
            };
        });
    }
    else {
        let sensor: Sensor|null = await prisma.sensor.findFirst({where: {id: req.body.sensorId}});
        if(sensor === null) return res.status(404).end();
        data = [{
            id: sensor.id,
            label: sensor.label,
            contextUnit: sensor.contextUnit,
            type: sensor.typeName
        }];
    }

    return res.status(200).json({
        status: 200,
        ok: true,
        body: data
    });
};