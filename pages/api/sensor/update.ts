// TODO: update sensor & contextUnit /w sensorId & apiToken
import {NextApiRequest, NextApiResponse} from "next";
import {ResponseDto} from "../../../models/dto/ResponseDto";
import {prisma} from "../../../services/prisma";
import bcrypt from "bcrypt";
import { User } from "@prisma/client";
import {SensorTypeEnum} from "../../../models/SensorTypeEnum";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto>
) {
    if (req.method != "PATCH") return res.status(405).setHeader("Allow", "PATCH").end();
    if (typeof req.body.sensorId !== 'number'
     || typeof req.body.sensorData !== 'object'
     || !(!req.body.sensorData.type || typeof req.body.sensorData.type !== 'string')
     || !(!req.body.sensorData.type || Object.values(SensorTypeEnum).includes(req.body.sensorData.type))
    ) return res.status(400).end();

    if (!req.headers.authorization) return res.status(401).setHeader("WWW-Authenticate", "Token").end();
    const [scheme, token] = req.headers.authorization.split(' ');
    const userQuery = {where: {apiToken: token}};
    const user: User|null = await prisma.user.findFirst(userQuery);

    if(user !== null && scheme === 'Token' && !!token) {
        try {
            await prisma.sensor.update({
                where: {id: req.body.sensorId},
                data: {
                    label: req.body.sensorData.label,
                    contextUnit: req.body.sensorData.contextUnit,
                    type: req.body.sensorData.type
                }
            });
        } catch(e: any) {
            return res.status(404).json({
                status: 404,
                ok: false
            });
        }

        return res.status(200).json({
            status: 200,
            ok: true
        });
    }

    return res.status(401).setHeader("WWW-Authenticate", "Token").end();
}