// TODO: get apiToken /w pwd & name
import { NextApiRequest, NextApiResponse } from 'next';
import bcrypt from 'bcrypt';
import { User } from '@prisma/client';
import { prisma } from '../../services/prisma';
import {ResponseDto} from "../../models/dto/ResponseDto";
import {TokenDto} from "../../models/dto/TokenDto";
import crypto from "crypto";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<TokenDto>>
) {
    if (req.method != "POST") return res.status(405).setHeader("Allow", "POST").end();
    if (typeof req.body !== 'object') return res.status(400).end();
    if (typeof req.body.name !== 'string'
     || typeof req.body.pwd !== 'string'
    ) return res.status(400).end();

    const userQuery = {where: {name: req.body.name}};
    const user: User|null = await prisma.user.findUnique(userQuery);

    if(user !== null) {
        if(await bcrypt.compare(req.body.pwd, user.password)) {
            const newApiToken = crypto.randomBytes(20).toString('hex');
            await prisma.user.update({
                ...userQuery,
                data: {
                    apiToken: newApiToken
                }
            });

            return res.status(200).json({
                status: 200,
                ok: true,
                body: {
                    token: newApiToken
                }
            });
        }
    }

    return res.status(401).end();
};