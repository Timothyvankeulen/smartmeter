// TODO: get sensorLabel, sensorType, measurement, contextUnit & createAt /w or /w-out sensorId
import { SensorData } from "@prisma/client";
import {NextApiRequest, NextApiResponse} from "next";
import {ResponseDto} from "../../models/dto/ResponseDto";
import {prisma} from '../../services/prisma';
import {SensorDataDto} from "../../models/dto/SensorDataDto";
import bcrypt from "bcrypt";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<SensorDataDto[]>>
) {
    if (req.method != "DELETE") return res.status(405).setHeader("Allow", "DELETE").end();

    const ip = req.headers["x-real-ip"] || req.connection.remoteAddress;
    if(ip != '::1') return res.status(403).end();

    try {
        await prisma.sensorData.deleteMany();
        await prisma.user.deleteMany();
        await prisma.user.create({
            data: {
                name: 'admin',
                password: await bcrypt.hash('admin', 10)
            }
        });

        return res.status(200).json({
            status: 200,
            ok: true
        });
    } catch(e) {
        return res.status(500).json({
            status: 500,
            ok: false
        });
    }
}