// TODO: get name /w apiToken
import { User } from "@prisma/client";
import {NextApiRequest, NextApiResponse} from "next";
import {ResponseDto} from "../../../models/dto/ResponseDto";
import {prisma} from "../../../services/prisma";
import {UserDto} from "../../../models/dto/UserDto";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<UserDto>>
) {
    if (req.method != "GET") return res.status(405).setHeader("Allow", "GET").end();

    if (!req.headers.authorization) return res.status(401).setHeader("WWW-Authenticate", "Token").end();
    const [scheme, token] = req.headers.authorization.split(' ');
    const userQuery = {where: {apiToken: token}};
    const user: User|null = await prisma.user.findFirst(userQuery);

    if(user !== null && scheme === 'Token' && !!token) {
        return res.status(200).json({
            status: 200,
            ok: true,
            body: {
                name: user.name
            }
        });
    }

    return res.status(401).setHeader("WWW-Authenticate", "Token").end();
}