// TODO: update name & pwd /w apiToken
import { User } from "@prisma/client";
import {NextApiRequest, NextApiResponse} from "next";
import {ResponseDto} from "../../../models/dto/ResponseDto";
import {prisma} from "../../../services/prisma";
import {UserDto} from "../../../models/dto/UserDto";
import bcrypt from "bcrypt";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<ResponseDto<UserDto>>
) {
    if (req.method != "PATCH") return res.status(405).setHeader("Allow", "PATCH").end();
    if(typeof req.body !== 'object') return res.status(400).end();

    if (!req.headers.authorization) return res.status(401).setHeader("WWW-Authenticate", "Token").end();
    const [scheme, token] = req.headers.authorization.split(' ');
    const userQuery = {where: {apiToken: token}};
    const user: User|null = await prisma.user.findFirst(userQuery);

    if(user !== null && scheme === 'Token' && !!token) {
        let hash;
        if(!!req.body.pwd) hash = await bcrypt.hash(req.body.pwd, 10);
        await prisma.user.update({
            where: {id: user.id},
            data: {
                name: req.body.name,
                password: hash
            }
        });

        return res.status(200).json({
            status: 200,
            ok: true
        });
    }

    return res.status(401).setHeader("WWW-Authenticate", "Token").end();
}