import { NextPage } from 'next';
import * as React from "react";
import styles from "./login.module.css";
import {useState} from "react";
import {ApiManager} from "../services/ApiManager";
import {useRouter} from "next/router";

const Login: NextPage = () => {
    const router = useRouter();
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const login = () => {
        ApiManager.login(username, password)
            .then((response) => {
                router.push('/profile');
            })
            .catch((err) => {
                alert('Login credentials are incorrect!');
            });
    };

    return (
        <div className={styles['container']}>
            <div className={styles['login-container']}>
                <label className={styles['input-container']}>
                    <span className={styles['label']}>Username:</span>
                    <input type="text" placeholder="Username"
                           className={styles['input']}
                           value={username}
                           onChange={(e) => setUsername(e.target.value)}
                    />
                </label>
                <label className={styles['input-container']}>
                    <span className={styles['label']}>Password:</span>
                    <input type="password" placeholder="Password"
                           className={styles['input']}
                           value={password}
                           onChange={(e) => setPassword(e.target.value)}
                    />
                </label>
                <button onClick={login} className={styles['login-btn']}>Login</button>
            </div>
        </div>
    );
};

export default Login;
