import { NextPage } from "next";
import { useEffect, useState } from "react";
import React from "react";
import {Line} from "react-chartjs-2";
import { ApiManager } from "../../services/ApiManager";
import {
    Chart as ChartJS,
    PointElement,
    LineElement
} from "chart.js";
ChartJS.register(
    PointElement,
    LineElement
);

const RealtimeLineChart: NextPage = () => {
    const sensorColors = [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
        'rgba(255, 206, 86, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)'
    ];
    const recentHistoryLabels = Array(61).fill(undefined).map((_,i) => {
        switch(i) {
            case 0: return 'Now';
            case 15: return '15m ago';
            case 30: return '30m ago';
            case 45: return '45m ago';
            case 60: return '1h ago';
            default: return `${i}m`;
        }
    });
    console.log(recentHistoryLabels);
    const [data, setData] = useState<any>();
    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: "top" as const,
            },
            title: {
                display: true,
                text: "Realtime Wh in the last hour",
            },
        },
    };

    useEffect(() => {
        ApiManager.getSensorData().then((res) => {
            const datasetAcumulator: {[sensorLabel: string]: {[month: number]: {entries: number, total: number}}} = {};
            const currentDate = new Date();
            if(res.body) {
                res.body.map(x => {
                    x.createdAt = new Date(x.createdAt);
                    return x;
                }).sort(x => x.createdAt.getTime()).forEach((data, i) => {
                    if(currentDate.getTime() - data.createdAt.getTime() > 1000*60*60) return;

                    const label = data.sensor.label;
                    if(!(label in  datasetAcumulator)) datasetAcumulator[label] = {};

                    const minute = Math.floor((currentDate.getTime() - data.createdAt.getTime())/60000);
                    if(minute in datasetAcumulator[label]) {
                        datasetAcumulator[label][minute].total += data.measurement*data.contextUnit;
                        datasetAcumulator[label][minute].entries++;
                        return;
                    }
                    datasetAcumulator[label][minute] = {entries: 1, total: data.measurement}
                });
            }

            const displayableLabels: number[] = [];
            const dataset = Object.keys(datasetAcumulator).map((label) => {
                Object.keys(datasetAcumulator[label]).forEach((timeframe: any) => {
                    displayableLabels.push(timeframe);
                });
                return {
                    label: label,
                    data: Object.values(datasetAcumulator[label]).map(x => x.total/x.entries)
                };
            });


            setTimeout(() => {
                setData({
                    labels: displayableLabels.sort().map(x => recentHistoryLabels[x]),//Array.from(recentHistoryLabels).splice(-1, Math.max(...minutesRecorded.map(Number))),
                    datasets: dataset.map((entry, idx) => {
                        return {
                            label: entry.label,
                            data: entry.data,
                            backgroundColor: sensorColors[idx]
                        };
                    })
                });
            }, 2000);
        });
    }, [data]);

    return (
        <div style={{ height: 500, width: 500 }}>
            {data && <Line options={options} data={data} />}
        </div>
    );
};

export default RealtimeLineChart;
