import * as d3 from "d3";
import { NextPage } from "next";
import { useEffect, useState } from "react";
import { SensorData } from "@prisma/client";
import { prisma } from "../../services/prisma";

import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { ApiManager } from "../../services/ApiManager";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);


const BarGraph: NextPage = () => {
  const sensorColors = [
    'rgba(255, 99, 132, 0.5)',
    'rgba(54, 162, 235, 0.5)',
    'rgba(255, 206, 86, 0.5)',
    'rgba(75, 192, 192, 0.5)',
    'rgba(153, 102, 255, 0.5)'
  ];
  const monthLabels = ["January", "February", "March", "April", "May", "June", "Juli", "Augustus", "September", "October", "November", "December"];
  const [data, setData] = useState<any>();
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top" as const,
      },
      title: {
        display: true,
        text: "Measurement Ampere (A) - per month",
      },
    },
  };

  useEffect(() => {
    ApiManager.getSensorData().then((res) => {
      const datasetAcumulator: {[sensorLabel: string]: {[month: number]: {entries: number, total: number, deltaTime: number}}} = {};
      const currentDate = new Date();
      if(res.body) {
        res.body.map(x => {
          x.createdAt = new Date(x.createdAt);
          return x;
        }).sort(x => x.createdAt.getTime()).forEach((data, i) => {
          if(currentDate.getFullYear() != data.createdAt.getFullYear()) return;

          let deltaTime = 0;
          // @ts-ignore
          if(i != 0 && res.body[i-1].createdAt.getMonth() == data.createdAt.getMonth()) deltaTime += (data.createdAt.getTime() - res.body[i-1].createdAt.getTime()) / 2;
          // @ts-ignore
          if(i != res.body.length-1 && res.body[i+1].createdAt.getMonth() == data.createdAt.getMonth()) deltaTime += (res.body[i+1].createdAt.getTime() - data.createdAt.getTime()) / 2;

          const label = data.sensor.label;
          if(!(label in  datasetAcumulator)) datasetAcumulator[label] = {};

          const month = data.createdAt.getMonth();
          if(month in datasetAcumulator[label]) {
            datasetAcumulator[label][month].total += data.measurement*deltaTime;
            datasetAcumulator[label][month].entries++;
            datasetAcumulator[label][month].deltaTime += deltaTime;
            return;
          }
          datasetAcumulator[label][month] = {entries: 1, total: data.measurement, deltaTime}
        });
      }
      const dataset = Object.keys(datasetAcumulator).map((label) => {
        return {
          label: label,
          data: Object.values(datasetAcumulator[label]).map(x => x.total/(x.deltaTime))
        };
      });

      // @ts-ignore
      const monthsRecorded: string[] = [...new Set(Object.values(datasetAcumulator).map(x => Object.keys(x)).flat())];
      setData({
        labels: Array.from(monthLabels).splice(Math.min(...monthsRecorded.map(Number)), dataset.length),
        datasets: dataset.map((entry, idx) => {
          return {
            label: entry.label,
            data: entry.data,
            backgroundColor: sensorColors[idx]
          };
        })
      });
    });
  }, []);

  return (
    <div style={{ height: 500, width: 500 }}>
      {data && <Bar options={options} data={data} />}
    </div>
  );
};

export default BarGraph;
