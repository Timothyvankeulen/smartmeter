export type ResponseDto<TBody=never> = {
    status: number,     // Status code
    ok: boolean         // Successful request
    body?: TBody,       // Response body
    message?: string,   // User message
    error?: any         // Dev message
};