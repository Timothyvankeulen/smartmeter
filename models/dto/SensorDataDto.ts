export type SensorDataDto = {
    measurement: number,
    contextUnit: number,
    createdAt: Date,
    sensor: {
        label: string,
        typeName: string
    }
};