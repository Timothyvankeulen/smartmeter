export type SensorDto = {
    id: number,
    label: string,
    contextUnit: number,
    type: string
};