import {ResponseDto} from '../models/dto/ResponseDto';
import {TokenDto} from "../models/dto/TokenDto";
import {UserDto} from "../models/dto/UserDto";
import {SensorDto} from "../models/dto/SensorDto";
import {SensorDataDto} from "../models/dto/SensorDataDto";

export class ApiManager {
    public static HOST: string = 'http://smeter.local:80';
    private static ApiToken?: string;
    private static ApiTokenChangedEvent: (()=>void)[] = [];
    public static subscribeTokenApiChange = (callback: ()=>void) => ApiManager.ApiTokenChangedEvent.push(callback);
    private static callApiTokenChangedEvent = () => ApiManager.ApiTokenChangedEvent.forEach(e => e());

    public static isAuthenticated = () => !!ApiManager.getApiToken();
    private static getApiToken = () => {
        return ApiManager.ApiToken || window.localStorage.getItem('api-token');
    };
    private static setApiToken = (apiToken?: string) => {
        ApiManager.ApiToken = apiToken;
        if(!apiToken) window.localStorage.removeItem('api-token');
        else window.localStorage.setItem('api-token', apiToken);
    };

    private static async request<TResponseBody>(uri: string, method: 'GET'|'POST'|'PATCH'|'DELETE' = 'GET', data?: any): Promise<ResponseDto<TResponseBody>> {
        return await (await fetch(`${ApiManager.HOST}/api${uri}`, {
            method: method,
            cache: 'no-cache',
            body: JSON.stringify(data),
            headers: {
                'Authorization': ApiManager.getApiToken() ? `Token ${ApiManager.getApiToken()}` : '',
                'Content-Type': data ? 'application/json': ''
            }
        })).json();
    }

    public static async login(username: string, password: string) {
        const response = await ApiManager.request<TokenDto>('/login', 'POST', {
            name: username,
            pwd: password
        });

        if(response.ok && response.body) ApiManager.setApiToken(response.body.token);
        ApiManager.callApiTokenChangedEvent();
        return response;
    }

    public static async logout() {
        ApiManager.setApiToken(undefined);
        ApiManager.callApiTokenChangedEvent();
        return {
            status: 200,
            ok: true
        };
    }

    public static async getUserName() {
        return await ApiManager.request<UserDto>('/user');
    }

    public static async updateUser(data: {username: string, password?: string}) {
        return await ApiManager.request('/user/update', 'PATCH', data);
    }

    public static async getSensors() {
        return await ApiManager.request<SensorDto[]>('/sensor');
    }

    public static async updateSensor(sensorId: number, data: {label: string, contextUnit: number}) {
        return await ApiManager.request('/sensor/update', 'PATCH', {
            sensorId: sensorId,
            sensorData: data
        });
    }

    public static async getSensorData(sensorId?: number) {
        return await ApiManager.request<SensorDataDto[]>(`/sensor-data?sensorId=${sensorId ?? ''}`);
    }
}
